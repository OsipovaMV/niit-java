package Staff;

public class ProjectManager extends Manager implements Heading
{
    final static int paymentForProgrammers = 100;
    public ProjectManager(int id, String name){
        super(id, name);
    }

    public int calcPayment(){
        return calcHeadingPayment() + calcProjectPayment();
    }

    public int calcProjectPayment(){
        return workProject.price * workProject.percentForManager / 100;
    }

    public int calcHeadingPayment(){
        return workProject.getNumOfEmployees() * paymentForProgrammers;
    }
}
