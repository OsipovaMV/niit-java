package Staff;

import java.io.*;

public class StaffDemo
{
    public static Employee[] loadEmployeesFromFile(String file)
    {
        Employee employees[];
        try
        {
            //Ищем количество строк в файле для определения размера массива
            LineNumberReader  lnr = new LineNumberReader(new FileReader(file));
            lnr.skip(Long.MAX_VALUE);
            employees = new Employee[lnr.getLineNumber() + 1];
            lnr.close();

            //String buf;
            //String parsedBuf[];
            //bufferedReader.mark((int)f.getUsableSpace());
            File f = new File(file);
            FileReader fr = new FileReader(f);
            BufferedReader bufferedReader = new BufferedReader(fr);
            String line;
            String lineItems[];
            int i = 0;
            while ((line = bufferedReader.readLine()) != null){
                lineItems = line.split(",");
                int id = Integer.parseInt(lineItems[0]);
                String name = lineItems[1];
                String position = lineItems[2];
                int payment = Integer.parseInt(lineItems[3]);
                switch (position){
                    case "Driver":
                        employees[i] = new Driver(id,name,40,payment);
                        break;
                    case "Cleaner":
                        employees[i] = new Cleaner(id,name,40,payment);
                        break;
                    case "Programmer":
                        employees[i] = new Programmer(id,name,40,payment,1);
                        break;
                    case "Tester":
                        employees[i] = new Tester(id,name,40,payment,1);
                        break;
                    case "TeamLeader":
                        employees[i] = new TeamLeader(id,name,40,payment,2,7);
                        break;
                    case "ProjectManager":
                        employees[i] = new ProjectManager(id,name);
                        break;
                    case "SeniorManager":
                        employees[i] = new SeniorManager(id,name);
                        break;
                    default:
                        throw new IOException("Error: Wrong file format!");
                }
                i++;
            }
            return employees;
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String args[]){
        WorkProject proj = new WorkProject("My_proekt", 100000, 4);
        Employee employees[] = loadEmployeesFromFile("D:\\Staff.txt");
        for (Employee employee : employees)
            proj.addEmployee(employee);
        proj.printSalary();
    }
}