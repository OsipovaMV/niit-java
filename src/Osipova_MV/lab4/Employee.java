package Staff;

public abstract class Employee
{
    protected int id;
    protected String name;
    protected int workTime;
    protected int payment;

    public Employee(int id, String name, int workTime, int payment)
    {
        this.id = id;
        this.name = new String(name);
        this.workTime = workTime;
        this.payment = payment;
    }
    public abstract int calcPayment();
}