import java.io.*;
import java.net.*;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) throws IOException {
        PrintWriter out = null;
        ServerSocket server = null;
        Socket client = null;

        // create server socket
        try {
            server = new ServerSocket(55555);
        } catch (IOException e) {
            System.out.println("Couldn't listen to port 55555");
            System.exit(-1);
        }
        System.out.println("Server is waiting for a clients...");
        while(true) {
            try {
                client = server.accept();
            } catch (IOException e) {
                System.out.println("Can't accept");
                System.exit(-1);
            }
            LocalTime lt = LocalTime.now();
            System.out.println("Got new client. Sending time " + lt.getHour()+":"+lt.getMinute()+":"+lt.getSecond());
            out = new PrintWriter(client.getOutputStream(), true);
            out.println(lt.getHour()+":"+lt.getMinute()+":"+lt.getSecond());
            out.close();
            client.close();
        }
    }
}
