import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) throws IOException{
        JFrame frame = new JFrame();
        JPanel mainPanel = new JPanel();

        final JLabel label = new JLabel();
        JButton btnGetTime = new JButton("Get time");
        btnGetTime.setBounds(400, 50, 150, 100);
        label.setBounds(50, 50, 300, 100);
        btnGetTime.setFont(new Font("Arial", Font.PLAIN, 30));
        label.setFont(new Font("Arial", Font.PLAIN, 70));

        //frame.setLayout(null);
        mainPanel.setLayout(null);
        mainPanel.add(btnGetTime);
        mainPanel.add(label);
        frame.add(mainPanel);

        frame.setSize(600,250);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        btnGetTime.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    label.setText(getTimeFromServer());
                }catch (IOException exc) {
                    label.setText("Error");
                }
            }
        });
    }

    public static String getTimeFromServer() throws IOException {
        Socket server = null;
        System.out.println("Connecting to server... ");
        try {
            server = new Socket("localhost", 55555);
        } catch (IOException e) {
            System.out.println("Couldn't connect to server :(");
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
        String serverAnswer;
        serverAnswer = in.readLine();
        System.out.println("Time from server: " + serverAnswer);
        in.close();
        server.close();
        return serverAnswer;
    }
}
