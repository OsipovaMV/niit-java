//Написать программу, которая принимает через командную строку несколько числовых диапазонов
// и выдает на экран список чисел. Например: ''1,2,4-7,18-21'' -> 1,2,4,5,6,7,18,19,20,21.
public class Range_deploy {
    public static void main(String args[])// в качестве аргумента передаем массив строк с названием args
    {
        int i = 0;
        int buf = 0;
        int next = 0;
        char arr_new[] = args[0].toCharArray(); //метод toCharArray преобразует данную строку в новый массив символов
        int length_arr_new = arr_new.length; // метод length() возвращает длину последовательности символов
        for(i=0;i < length_arr_new;i++)
        {
            if(Character.isDigit(arr_new[i]))// метод Character.isDigit() определяет, является ли указанное значение типа char цифрой
            {
                buf = arr_new[i] - '0'+ buf * 10;// переводим символ  в число
            }
            else if(arr_new[i] == ',')//если запятая, то печатаем число и запятую
            {
                System.out.print(buf + ",");
                buf = 0;
            }
            else // если не число и не запятая смотрим следующий символ
                {
                    i++;
                    while(i < length_arr_new && Character.isDigit(arr_new[i]))//следующий символ не выходит за диапазон и является цифрой
                        next = arr_new[i++] - '0' + next * 10;
                    while(buf < next)// пока текущее число меньше последующего печатаем числа из диапазона
                        System.out.print(buf++ + ",");
                next = 0;
            }
        }
        System.out.println(buf);
    }
}
