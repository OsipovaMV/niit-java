//Написать программу, осуществляющую свертку числовых диапазонов
import java.io.*;

public class Range_undeploy {
    public static void main(String args[])// в качестве аргумента передаем массив строк с названием args
    {
        int buf = 0;//текущее число
        int next = 0;//следующее число
        boolean status = false;
        //создаем новый массив строк без запятой
        String arr_new[] = args[0].split(",");// метод split разделяет строку на подстроки по разделителю))
        int length_arr_new = arr_new.length; // метод length() возвращает длину последовательности символов
        int numArr[] = new int[length_arr_new];//создаем новый массив типа int размером как arr_new[]
        for (int i = 0; i < length_arr_new; i++) {
        numArr[i] = Integer.parseInt(arr_new[i]);
    }
        System.out.print(numArr[0]);

        for (int i = 0; i < numArr.length - 1; i++) {
            buf = numArr[i];
            next = numArr[i + 1];

            if (buf != (next - 1)) {
                if (status == true) {
                    System.out.print(buf);
                }
                System.out.print("," + next);
                status = false;
            } else {
                if (status == false) {
                    System.out.print("-");
                }
                status = true;
            }
        }
        if (status == true) {
            System.out.print(next);
        }
    }
}

private class Nums
{
    static String strings[][] =
            {
                    {"  ***  ",
                            " *   * ",
                            " *   * ",
                            " *   * ",
                            " *   * ",
                            " *   * ",
                            "  ***  "},

                    {"   *   ",
                            " ***   ",
                            "   *   ",
                            "   *   ",
                            "   *   ",
                            "   *   ",
                            " ***** "},

                    {"  ***  ",
                            " *   * ",
                            "    *  ",
                            "   *   ",
                            "  *    ",
                            " *     ",
                            " ***** "},

                    {" ***** ",
                            "    *  ",
                            "   *   ",
                            "  **** ",
                            "     * ",
                            "     * ",
                            " ***** "},

                    {" *   * ",
                            " *   * ",
                            " *   * ",
                            " ***** ",
                            "     * ",
                            "     * ",
                            "     * "},

                    {" ***** ",
                            " *     ",
                            " *     ",
                            " ***** ",
                            "     * ",
                            "     * ",
                            " ***** "},

                    {"    *  ",
                            "   *   ",
                            "  *    ",
                            " ***** ",
                            " *   * ",
                            " *   * ",
                            " ***** "},

                    {" ***** ",
                            "    *  ",
                            "   *   ",
                            "  *    ",
                            "  *    ",
                            "  *    ",
                            "  *    "},

                    {" ***** ",
                            " *   * ",
                            " *   * ",
                            " ***** ",
                            " *   * ",
                            " *   * ",
                            " ***** "},

                    {" ***** ",
                            " *   * ",
                            " *   * ",
                            " ***** ",
                            "    *  ",
                            "   *   ",
                            "  *    "}
            };
}







