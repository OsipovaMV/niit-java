/**
 * Created by Masha on 26.03.2017.
 */
public class Collatz {
    public static long posl;//текущее количество элементов в последовательности
    public static long max;

    public static long collatz(long n) {
        posl++;
        if (n == 1)
            return n;
        if (n % 2 != 0)
            return collatz(3 * n + 1);//передаем в метод n
        else
            return collatz(n / 2);//передаем в метод n
    }


    public static void main(String[] args) {
        long i;//счетчик
        long max = 0;//максимальное число элементов в последовательности
        long n = 2;// число, при котором в последовательности максимальное часло элементов
        for (i = 2; i <= 1000000; i++) {
            posl = 0;
            collatz(i);
            if (posl > max) {
                max = posl;
                n = i;
            }
           // System.out.println("i= " + i+"текущее кол-во элементов" + posl);
        }
        System.out.println("Max Collatz " + max + " members, n = " + n);
    }
}

