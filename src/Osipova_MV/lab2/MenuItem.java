
enum Drinks{TEA,ESPRESSO,LATTE,CHOCOLATE};

public class MenuItem {
    public int Price;
    Drinks Drink;
    String ItemName;
    MenuItem(String itemName, Drinks drink, int price){
        Price = price;
        Drink = drink;
        ItemName = itemName;
    }
}
